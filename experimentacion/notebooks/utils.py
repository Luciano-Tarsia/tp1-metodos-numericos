import subprocess
import platform
import pandas as pd
import matplotlib.pyplot as plt

methods = ["CMM", "WP", "GOLES"]

def runAllMethods(input):
    for i in range(len(methods)):
        runMethod(i, input, f'../resultados/nba_2016_ranking_{i}.out')


def runMethod(method, input, output):
    ejecutable = "../../programaRankeo"
    system = platform.system()
    if system == "Windows":
        ejecutable = "../../programaRankeo.exe"
    subprocess.run([f"{ejecutable}", f"{method}", f"{input}", f"{output}"], stderr = subprocess.PIPE, encoding = 'ascii')

def getRanking(method):
    data = pd.read_csv("../data/nba_2016_teams.csv")
    ranking = pd.read_csv(f'nba_2016_ranking_{method}.out', delimiter = "\n", header=None)
    data['ranking'] = ranking
    return data

def getAllRankings():
    data = pd.read_csv("../data/nba_2016_teams.csv")
    for i in range (len(methods)):
        ranking = pd.read_csv(f'../resultados/nba_2016_ranking_{i}.out', delimiter = "\n", header=None)
        data[methods[i]] = ranking
    return data

def getRankingGanador(method):
    data = pd.read_csv("../data/nba_2016_teams_con_ganador.csv")
    ranking = pd.read_csv(f'../resultados/nba_2016_ranking_{method}.out', delimiter = "\n", header=None)
    data['ranking'] = ranking
    return data

def showScatterRanking(ranking, title):
    fig = plt.figure(figsize=(18,12), dpi= 60)
    ax1 =  plt.scatter(ranking.CMM, ranking.index, s=30, alpha=1, label='CMM', color='red')
    ax2 =  plt.scatter(ranking.WP, ranking.index, s=30, alpha=1, label='WP', color='green')
    ax2 =  plt.scatter(ranking.GOLES, ranking.index, s=30, alpha=1, label='GOLES', color='blue')

    plt.yticks(ranking.index, ranking.Equipo)
    plt.title(title, fontdict={'size':20})
    plt.xlabel('Ranking')
    plt.grid(linestyle='--', alpha=0.5)
    plt.xlim(-0.01, 1.01)
    plt.legend()
    plt.show()
