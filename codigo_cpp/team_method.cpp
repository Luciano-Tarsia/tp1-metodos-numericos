#include "definiciones.h"

vector<double> pointsPercentageMethod(const vector <Registro> &registros) {
    vector<double> result(registros.size());

    double min = registros[0].goles;
    double max = registros[0].goles;

    for (auto i : registros) {
        if (i.goles > max) {
            max = i.goles;
        }
        if (i.goles < min) {
            min = i.goles;
        }
    }

    for (int i = 0; i < registros.size(); i++) {
        result[i] = (registros[i].goles - min) / (max - min);
        if (max - min == 0) {
            result[i] = 1;
        }
    }

    return result;
}
