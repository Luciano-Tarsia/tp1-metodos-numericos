#ifndef TP1_DEFINICIONES_H
#define TP1_DEFINICIONES_H

#include <fstream>
#include <iostream>
#include <vector>
#include <map>
#include <chrono>
#include <iomanip>

using namespace std;

using matriz = vector <vector<double>>;

class Registro {
public:
    double goles;
    double jugados;
    double ganados;
    vector<double> enfretados;

    Registro(double cantEquipos) {
        goles = 0;
        jugados = 0;
        ganados = 0;
        enfretados = vector<double>(cantEquipos, 0);
    }
};

#endif //TP1_DEFINICIONES_H
