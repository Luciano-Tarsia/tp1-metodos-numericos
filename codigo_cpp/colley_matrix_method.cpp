#include "definiciones.h"
#include "metodos.h"

void eliminacionGaussiana(matriz &m, vector<double> &b) {
    double div = 0;
    int n = m.size();
    for (int i = 0; i < n - 1; i++) {
        for (int j = i + 1; j < n; j++) {
            div = (m[j][i] / m[i][i]);
            for (int k = i; k < n; k++) {
                m[j][k] = m[j][k] - (div * m[i][k]);
            }
            b[j] = b[j] - (div * b[i]);
        }
    }
}

vector<double> resultado(matriz &m, vector<double> &b) {
    int n = m.size();
    eliminacionGaussiana(m, b);
    vector<double> res;
    for (int i = 0; i < n; i++) {
        res.push_back(-1);
    }
    for (int i = n - 1; i >= 0; i--) {
        double aux = b[i];
        for (int j = i + 1; j < n; j++) {
            aux = aux - (m[i][j] * res[j]);
        }
        res[i] = aux / m[i][i];
    }
    return res;
}

vector<double> colleyMatrixMethod(const vector <Registro> &registro) {
    // Creamos la matriz de Colley
    matriz colleyMatrix(registro.size(), vector<double>(registro.size(), 0));
    for (int i = 0; i < registro.size(); i++) {
        for (int j = 0; j < registro.size(); j++) {
            if (i == j) {
                // En la diagonal
                colleyMatrix[i][j] = 2 + registro[i].jugados;
            } else {
                colleyMatrix[i][j] = -registro[i].enfretados[j];
            }
        }
    }

    vector<double> b(registro.size());
    for (int i = 0; i < registro.size(); ++i) {
        // 1 + (ganados - perdidos)/2
        b[i] = 1 + (registro[i].ganados - (registro[i].jugados - registro[i].ganados)) / 2;
    }

    return resultado(colleyMatrix, b);
}
