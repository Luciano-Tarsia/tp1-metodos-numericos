#include "definiciones.h"
#include "metodos.h"

int main(int argc, char **argv) {

    map <string, string> algoritmos_implementados = {
            {"0", "CMM"},
            {"1", "WP"},
            {"2", "Goles"},
    };

    // Verificar que el algoritmo pedido exista.
    if (argc != 4 or algoritmos_implementados.find(argv[1]) == algoritmos_implementados.end()) {
        cerr << "Algoritmo no encontrado: " << argv[1] << endl;
        cerr << "Los algoritmos existentes son: " << endl;
        for (auto &alg_desc: algoritmos_implementados)
            cerr << "\t- " << alg_desc.first << ": " << alg_desc.second << endl;
        return 1;
    }

    string metodo = argv[1];
    ifstream archivoEntrada(argv[2]);
    ofstream archivoSalida(argv[3]);

    if (!archivoEntrada.is_open()) {
        cout << "El archivo no se abrio correctamente" << endl;
        return 1;
    }

    double cantEquipos;
    double basura;
    archivoEntrada >> cantEquipos >> basura;

    vector <Registro> infoEquipos(cantEquipos, Registro(cantEquipos));

    double nombreEquipo1,golesEquipo1,nombreEquipo2,golesEquipo2 = 0;

	archivoEntrada >> basura;
    while (!archivoEntrada.eof()) {
        archivoEntrada >> nombreEquipo1 >> golesEquipo1 >> nombreEquipo2 >> golesEquipo2;

        // Guardamos la cantidad de partidos jugados del equipo
        infoEquipos[nombreEquipo1 - 1].jugados++;
        infoEquipos[nombreEquipo2 - 1].jugados++;
		
        // Guardamos la cantidad de enfrentamientos entre los competidores
        infoEquipos[nombreEquipo1 - 1].enfretados[nombreEquipo2 - 1]++;
        infoEquipos[nombreEquipo2 - 1].enfretados[nombreEquipo1 - 1]++;

        // Guardamos la cantidad de goles del equipo
        infoEquipos[nombreEquipo1 - 1].goles += golesEquipo1;
        infoEquipos[nombreEquipo2 - 1].goles += golesEquipo2;

        if (golesEquipo1 > golesEquipo2) {
            // Guardamos la cantidad de partidos ganados del Equipo1 (Si es que ganó)
            infoEquipos[nombreEquipo1 - 1].ganados++;
        } else {
            // Guardamos la cantidad de partidos ganados del Equipo2 (Si es que ganó)
            infoEquipos[nombreEquipo2 - 1].ganados++;
        }

		archivoEntrada >> basura;
    }

    archivoEntrada.close();

    vector<double> ranking;

    auto inicioCrono = chrono::steady_clock::now();

    if (metodo == "0") {
        ranking = colleyMatrixMethod(infoEquipos);
    } else if (metodo == "1") {
        ranking = winPercentageMethod(infoEquipos);
    } else {
        ranking = pointsPercentageMethod(infoEquipos);
    }

    auto finCrono = chrono::steady_clock::now();
    clog << chrono::duration_cast<chrono::nanoseconds>(finCrono - inicioCrono).count() << endl;

    for (auto i : ranking) {
        archivoSalida << setprecision(14) << i << endl;
    }

    archivoSalida.close();

    return 0;
}
