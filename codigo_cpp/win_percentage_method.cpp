#include "definiciones.h"

vector<double> winPercentageMethod(const vector<Registro> &registros) {
    vector<double> result(registros.size());
    for (int i = 0; i < registros.size(); ++i) {
        result[i] = registros[i].ganados / registros[i].jugados;
    }
    return result;
}