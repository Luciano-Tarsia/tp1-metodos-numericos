#ifndef CODIGO_METODOS_H
#define CODIGO_METODOS_H

vector<double> winPercentageMethod(const vector <Registro> &registros);

void eliminacionGaussiana(matriz &m, vector<double> &b);

vector<double> resultado(matriz &m, vector<double> &b);

vector<double> colleyMatrixMethod(const vector <Registro> &registro);

vector<double> pointsPercentageMethod(const vector <Registro> &registros);

#endif
