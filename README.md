# TP1 - Métodos Numéricos

## Compilación
Se proporciona un script que compila el codigo C++
```
./compilarCodigo.sh
```

### Metodos disponibles

| Código | Método |
| - | - |
| 0 | CMM |
| 1 | WP |
| 2 | Goles |

## Ejecución

```
./programaRankeo [codigoDeMetodo] [archivoEntrada] [archivoSalida]
```

- `archivoEntrada` path del archivo con los datos de entrada
- `archivoSalida` path del archivo donde se almacenaran los resultados

